package com.example.windowfabdispatch.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.windowfabdispatch.R;
import com.example.windowfabdispatch.models.DeliveryData;

import java.util.List;

public class MyDeliverAdapter extends RecyclerView.Adapter<MyDeliverAdapter.MyDeliverHolder> {

    List<DeliveryData> list;
    Context context;
    MyDeliveryListener listener;

    public MyDeliverAdapter(List<DeliveryData> list, Context context) {
        this.list = list;
        this.context = context;
    }
public interface MyDeliveryListener{
        void onMyDeliveryClick(int pos);
}
    @NonNull
    @Override
    public MyDeliverHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyDeliverHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mydelivery_row,null));
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyDeliverHolder myDeliverHolder, final int i) {
final DeliveryData data=list.get(i);
//myDeliverHolder.packageCost.setText(data.getPackageCost());
myDeliverHolder.packageName.setText(data.getPackageName());

if (data.getPackageStatus().equals("Completed")){
    myDeliverHolder.PackageStatus.setTextColor(R.color.light_green);

}
myDeliverHolder.delivery_card.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
//        if (data.getPackageStatus().equals("Completed")){
//            listener.onMyDeliveryClick(i);
//        }
        listener.onMyDeliveryClick(i);
    }

});

        myDeliverHolder.PackageStatus.setText(data.getPackageStatus());
myDeliverHolder.PackageTime.setText(data.getPackageTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
public void setMyDeliveryListener(MyDeliveryListener listener1){
        this.listener=listener1;
}
    class MyDeliverHolder extends RecyclerView.ViewHolder{
TextView packageName,packageCost,PackageTime,PackageStatus;
CardView delivery_card;
        public MyDeliverHolder(@NonNull View itemView) {
            super(itemView);
            packageName=itemView.findViewById(R.id.packageName);
//                    packageCost=itemView.findViewById(R.id.packageCost);
                    PackageTime=itemView.findViewById(R.id.PackageTime);
                    PackageStatus=itemView.findViewById(R.id.PackageStatus);
            delivery_card=itemView.findViewById(R.id.delivery_card);
        }
    }

}
