package com.example.windowfabdispatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windowfabdispatch.MainActivity;
import com.example.windowfabdispatch.R;
import com.example.windowfabdispatch.constants.LinksAndKeys;
import com.example.windowfabdispatch.session.Session;
import com.example.windowfabdispatch.webservices.WebServiceController;
import com.example.windowfabdispatch.webservices.WebServiceInterface;
import com.google.gson.Gson;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements WebServiceInterface {

    Session session;
    private Toolbar toolbar;
    EditText editTextEmail, editTextPassword;
    Button buttonNext;
    private int LOGIN_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValidEmail(editTextEmail.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Please enter a valid Email", Toast.LENGTH_SHORT).show();
                } else if (editTextPassword.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter a Password", Toast.LENGTH_SHORT).show();
                } else {
                    Intent in = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(in);
//                    sendLoginRequest(editTextEmail.getText().toString(), editTextPassword.getText().toString());

                }
            }
        });
    }

    private void init() {

        session = new Session(LoginActivity.this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView textViewToolbarTitle = toolbar.findViewById(R.id.textViewToolbarTitle);
        textViewToolbarTitle.setText("Windowfab Sales");
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonNext = findViewById(R.id.buttonNext);
    }

    private void sendLoginRequest(String email, String password) {
        HashMap<String, String> paramsList = new HashMap<>();
        paramsList.put(LinksAndKeys.EMAIL_KEY, email);
        paramsList.put(LinksAndKeys.PASSWORD_KEY, password);

        WebServiceController webServiceController = new WebServiceController(LoginActivity.this, LoginActivity.this);
        String hitURL = LinksAndKeys.LOGIN_URL;
        webServiceController.sendPOSTRequest("", LinksAndKeys.REQUEST_MESSAGE, hitURL, paramsList, LOGIN_REQUEST_CODE);
    }

    @Override
    public void getResponse(int responseCode, String responseString, String requestType, int requestCode) {
//        if (requestCode == LOGIN_REQUEST_CODE && responseCode == 200) {
//
//            try {
//                Gson gson = new Gson();
//                UserLogin userLogin = gson.fromJson(responseString, UserLogin.class);

//                session.setUser(userLogin.getData().getUser());

                Intent in = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                finish();
//            } catch (Exception e) {
//                Toast.makeText(LoginActivity.this, getResources().getString(R.string.unable_to_login), Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
