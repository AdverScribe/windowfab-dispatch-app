package com.example.windowfabdispatch.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.example.windowfabdispatch.MainActivity;
import com.example.windowfabdispatch.R;
import com.example.windowfabdispatch.session.Session;
//import com.windowfab.sales.R;
//import com.windowfab.sales.session.Session;


/**
 * Created by Akshat-Lappy on 14/07/2016.
 */

public class SplashActivity extends Activity {

    Session session;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 1;
    int hasCoarseLocationPermission = 0;
    int hasFineLocationPermission = 0;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        session = new Session(SplashActivity.this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasCoarseLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            hasFineLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);

            if (hasCoarseLocationPermission != PackageManager.PERMISSION_GRANTED ||
                    hasFineLocationPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CALL_LOG,
                                Manifest.permission.WRITE_CALL_LOG},
                        REQUEST_CODE_ASK_PERMISSIONS);

                return;
            }
        } else {
            startThread();
        }

        startThread();
    }

    public void startThread() {
        session = new Session(SplashActivity.this);

        Thread mSplashThread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(1000);
                    }
                } catch (InterruptedException ex) {

                }

//                finish();

//                if (session.getUser() != null && !(session.getUser().getName().isEmpty())) {
                    Intent in = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                    finish();
//                } else {
//                    Intent in = new Intent(SplashActivity.this, LoginActivity.class);
//                    startActivity(in);
//                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                    finish();
//                }
            }
        };

        mSplashThread.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults.length>0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startThread();
                } else {
                    Toast.makeText(SplashActivity.this, "Please Grant Permissions.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
