package com.example.windowfabdispatch.webservices;

public interface WebServiceInterface {

	public void getResponse(int responseCode, String responseString, String requestType, int requestCode);

}
