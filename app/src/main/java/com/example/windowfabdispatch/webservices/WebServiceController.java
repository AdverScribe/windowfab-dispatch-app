package com.example.windowfabdispatch.webservices;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.example.windowfabdispatch.constants.LinksAndKeys;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class WebServiceController {

    Context context;
    private ProgressDialog progressDialog;
    WebServiceInterface myInterface;

    public WebServiceController(Context context, Object obj) {
        this.context = context;
        this.myInterface = (WebServiceInterface) obj;
    }

    public void sendPOSTRequest(String headerValue, String loaderMessage, String url, HashMap<String, String> paramsList, final int requestCode) {
        if (isNetworkAvailable()) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(loaderMessage);
            progressDialog.setCancelable(false);
            progressDialog.show();

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            if (!headerValue.equals(""))
                client.addHeader(LinksAndKeys.HEADER_KEY, "bearer " + headerValue);

            RequestParams params = new RequestParams();

            if (paramsList != null) {
                Iterator it = paramsList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    try {
                        params.put("" + pair.getKey(), "" + pair.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            client.setTimeout(60000);
            client.post(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub
                    progressDialog.dismiss();

                    String response = "";

                    try {
                        if (arg2 != null)
                            response = new String(arg2, "UTF-8");

                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();
                }

            });
        } else {
            Toast.makeText(context, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendGETRequest(String headerValue, String loaderMessage, String url, HashMap<String, String> paramsList, final int requestCode) {
        if (isNetworkAvailable()) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(loaderMessage);
            progressDialog.setCancelable(false);
            progressDialog.show();

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            if (headerValue != null && !headerValue.equals(""))
                client.addHeader(LinksAndKeys.HEADER_KEY, "bearer " + headerValue);

            RequestParams params = new RequestParams();

            if (paramsList != null) {
                Iterator it = paramsList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    try {
                        params.put("" + pair.getKey(), "" + pair.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            client.setTimeout(60000);
            client.get(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub
                    progressDialog.dismiss();

                    String response = "";

                    try {
                        if (arg2 != null)
                            response = new String(arg2, "UTF-8");

                        myInterface.getResponse(arg0, response, "GET", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        myInterface.getResponse(arg0, response, "GET", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();
                }

            });
        } else {
            Toast.makeText(context, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendSilentPOSTRequest(String headerValue, String url, HashMap<String, String> paramsList, final int requestCode) {

        if (isNetworkAvailable()) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();

            if (paramsList != null) {
                Iterator it = paramsList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    try {
                        params.put("" + pair.getKey(), "" + pair.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (!headerValue.equals(""))
                client.addHeader(LinksAndKeys.HEADER_KEY, "bearer " + headerValue);

            client.setTimeout(60000);


            client.post(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub

                    String response = "";

                    try {
                        if (arg2 != null)
                            response = new String(arg2, "UTF-8");

                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            });
        } else {
            Toast.makeText(context, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendSilentGETRequest(String headerValue, String url, HashMap<String, String> paramsList, final int requestCode) {

        if (isNetworkAvailable()) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();

            if (paramsList != null) {
                Iterator it = paramsList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    try {
                        params.put("" + pair.getKey(), "" + pair.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (!headerValue.equals(""))
                client.addHeader(LinksAndKeys.HEADER_KEY, "bearer " + headerValue);

            client.setTimeout(60000);

            client.get(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub

                    String response = "";

                    try {
                        if (arg2 != null)
                            response = new String(arg2, "UTF-8");

                        myInterface.getResponse(arg0, response, "GET", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        myInterface.getResponse(arg0, response, "GET", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            });
        } else {
            Toast.makeText(context, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendSplashRequest(String url, HashMap<String, String> paramsList, final int requestCode) {

        if (isNetworkAvailable()) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();

            if (paramsList != null) {
                Iterator it = paramsList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    try {
                        params.put("" + pair.getKey(), "" + pair.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            client.setTimeout(5000);

            client.get(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub

                    String response = "";

                    try {
                        if (arg2 != null)
                            response = new String(arg2, "UTF-8");

                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        myInterface.getResponse(arg0, response, "POST", requestCode);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            });
        } else {
            Toast.makeText(context, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void sendJSONBodyPOSTRequest(String headerValue, String url, StringEntity entity, final int requestID) {
        if (!isNetworkAvailable()) {
            Toast.makeText(context, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
        } else {

            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader(LinksAndKeys.HEADER_KEY, headerValue);

            client.setTimeout(30000);

            client.post(context, url, entity, "application/json", new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    // TODO Auto-generated method stub
                    try {
                        String response = "";
                        if (response != null || !response.equals("")) {
                            if (arg2 != null)
                                response = new String(arg2, "UTF-8");
                            else
                                response = "";
                        } else {
                            response = "";
                        }

                        System.out.println(response);
                        myInterface.getResponse(arg0, response, "POST", requestID);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    // TODO Auto-generated meth
                    try {
                        String response = new String(arg2, "UTF-8");
                        System.out.println(response);
                        myInterface.getResponse(arg0, response, "POST", requestID);
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
