package com.example.windowfabdispatch.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.windowfabdispatch.models.User;
import com.google.gson.Gson;



/**
 * Created by Aishvarya on 17-03-2017.
 */

public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setUser(User user) {
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("userObject", json);
        editor.commit();
    }

    public User getUser() {
        Gson gson = new Gson();
        String json = prefs.getString("userObject", "");
        User user = gson.fromJson(json, User.class);
        return user;
    }

    public void logout() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("userObject", "");
        editor.putString("otpDetails", "");
        editor.commit();
    }
}
