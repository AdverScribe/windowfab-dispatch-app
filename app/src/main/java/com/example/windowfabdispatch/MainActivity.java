package com.example.windowfabdispatch;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.windowfabdispatch.fragment.MyDeliveryFragment;
import com.example.windowfabdispatch.fragment.RHomeFragment;

public class MainActivity extends AppCompatActivity {

    FragmentTransaction fragmentTransaction;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout=findViewById(R.id.drawer);
        navigationView=findViewById(R.id.navigation_view);
        toolbar=findViewById(R.id.toolbar);
setSupportActionBar(toolbar);
getSupportFragmentManager().beginTransaction().addToBackStack(null)
        .add(R.id.frame_container,new RHomeFragment()).commit();
        TextView textViewToolbarTitle=toolbar.findViewById(R.id.textViewToolbarTitle);
        textViewToolbarTitle.setText("Window Fab Dispatch");

        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View headerLayout = navigationView.getHeaderView(0);

        TextView textViewName = headerLayout.findViewById(R.id.textViewName);
        TextView textViewPhone = headerLayout.findViewById(R.id.textViewPhone);
        TextView textViewEmail = headerLayout.findViewById(R.id.textViewEmail);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
switch (menuItem.getItemId()){
    case R.id.my_delivery:
        ReplaceFragment(new MyDeliveryFragment());
        return true;
    case R.id.menuHome:
        ReplaceFragment(new RHomeFragment());
        return true;
}
                return true;
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer,R.string.closeDrawer){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

  drawerLayout.setDrawerListener(actionBarDrawerToggle);
  actionBarDrawerToggle.syncState();

    }
    public void ReplaceFragment(Fragment fragment) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction
                .addToBackStack(getClass().getName())
                .commit();
    }
    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(Gravity.LEFT))
            drawerLayout.closeDrawers();
        else {
            int backStackEntry=getSupportFragmentManager().getBackStackEntryCount();
            if (backStackEntry>1){
                getSupportFragmentManager().popBackStack();
            }else {
                finish();
            }
        }
    }
}
