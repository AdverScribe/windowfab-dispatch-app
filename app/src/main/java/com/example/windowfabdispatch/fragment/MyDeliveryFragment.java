package com.example.windowfabdispatch.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.windowfabdispatch.R;
import com.example.windowfabdispatch.adapter.MyDeliverAdapter;
import com.example.windowfabdispatch.models.DeliveryData;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyDeliveryFragment extends Fragment {

RecyclerView delivery_list;
    List<DeliveryData> list=new ArrayList<>();

    public MyDeliveryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_delivery, container, false);
        delivery_list=view.findViewById(R.id.delivery_list);
        list.clear();
getData(list);
        MyDeliverAdapter adapter=new MyDeliverAdapter(list,getActivity());
        delivery_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        delivery_list.setAdapter(adapter);
        adapter.setMyDeliveryListener(new MyDeliverAdapter.MyDeliveryListener() {
            @Override
            public void onMyDeliveryClick(int pos) {

                    getFragmentManager().beginTransaction().addToBackStack(null)
                            .replace(R.id.frame_container,new DetailsFragment()).commit();

            }
        });
        return view;
    }

    private void getData(List<DeliveryData> list) {
        list.add(new DeliveryData("Banking Cards","200","2:05 pm","Pending"));
        list.add(new DeliveryData("KGF Tshirt","200","2:05 pm","Completed"));
        list.add(new DeliveryData("Banking Cards","200","2:05 pm","Pending"));
        list.add(new DeliveryData("Banking Cards","200","2:05 pm","Pending"));
    }

}
