package com.example.windowfabdispatch.fragment;


import android.Manifest;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.windowfabdispatch.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RHomeFragment extends Fragment {

    private static final int REQUEST_CODE = 112;
    Boolean hasPermission;
    TimePickerDialog dialog;
    Button editText;
    Button editText2;
    RecyclerView cll_logs;
    Bundle bundle;
    private  int i1 = 0,i2=0;
public static Date after_Date=new Date();
 Long times=null,times1=null;
//List<Cntct_Details> list=new ArrayList<>();
ProgressBar simpleProgressBar;
    public RHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivity().registerReceiver(bReceiver,new IntentFilter("message"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rhome, container, false);
        bundle=new Bundle();
        editText = view.findViewById(R.id.editText);
        simpleProgressBar = view.findViewById(R.id.simpleProgressBar);
        cll_logs = view.findViewById(R.id.cll_logs);
        editText2 = view.findViewById(R.id.editText2);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(getActivity(), "You denied the permission", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CALL_LOG}, REQUEST_CODE);
        }
//        if (times!=null && times1!=null){
//            cll_logs.setText(getContactDetails() + "time");
//        }
//list.clear();
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
if (i1==0){
   times=onStartTime();
    editText.setText("End Day");
    i1=1;
    return;
}
if (i1==1){
//    onshowDialog();
}

//                i1 = 1;
            }

        });
//        editText1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                i1 = 1;
//                i2=1;
////                showDialog(editText1);
//                list.clear();
//
//getContactDetails();
//            }
//        });
        editText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpleProgressBar.setVisibility(View.VISIBLE);
                if (times==null){
                    Toast.makeText(getActivity(),"Please Select Start Time", Toast.LENGTH_SHORT).show();
                }else if (times1==null){
                    Toast.makeText(getActivity(),"Please Select End Time", Toast.LENGTH_SHORT).show();
                }else {
                    simpleProgressBar.setVisibility(View.GONE);
//                    Fragment fragment=new TotalCallsFragment();
//                    bundle.putLong("start_time",times);
//                    bundle.putLong("end_time",times1);
//                    fragment.setArguments(bundle);
//                    getFragmentManager().beginTransaction().addToBackStack(null)
//                            .replace(R.id.frame_container,fragment)
//                            .commit();
                }
                simpleProgressBar.setVisibility(View.GONE);
            }
        });
//if (list!=null){
//    cll_logs.setLayoutManager(new LinearLayoutManager(getActivity()));
//    cll_logs.setAdapter(new CallsAdapter(list,getActivity()));
//}
//getContactDetails();
        return view;
    }

//    private void onshowDialog() {
//        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
//                .setTitle("Alert")
//                .setIcon(R.drawable.ic_warning)
//                .setMessage("Are you Sure You Want To End The Day?")
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//times1=onStartTime();
//                        System.out.println("assddffg"+times+"  "+times1);
//                    }
//                });
//        builder.show();
//    }

    private Long onStartTime() {
        Calendar calendar = Calendar.getInstance();
        System.out.println("assddffg"+calendar.getTimeInMillis());

//            times=calendar.getTimeInMillis();
return calendar.getTimeInMillis();
    }

    public void showDialog(final EditText edittext) {
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        String am_pm = "";
        System.out.println("assddffg" + mcurrentTime.get(Calendar.AM_PM) + mcurrentTime.get(Calendar.HOUR));
        if (mcurrentTime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (mcurrentTime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";

        dialog = new TimePickerDialog(getActivity(), R.style.mdtp_ActionButton_Text, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String finalAm_pm = (hourOfDay>12)?"PM":"AM";

                if (hourOfDay>0 && hourOfDay>12){
                     hourOfDay=hourOfDay-12;
                }

//                edittext.setText(hourOfDay + " : " + minute+finalAm_pm );
                if (i1==0){
                    edittext.setText("Start Time  "+hourOfDay + " : " + minute+finalAm_pm );
                    times=mcurrentTime.getTimeInMillis();

                }

                if (i1==1 && i2==1){
                    edittext.setText("End Time  "+hourOfDay + " : " + minute+finalAm_pm );
                    times1=mcurrentTime.getTimeInMillis();

//                    cll_logs.setText(getContactDetails() + "time");
//                    getContactDetails();
                }

                System.out.println("assddffg" +times+"  "+times1);
            }
        }, hour, minute, false);
        dialog.setTitle("Select Time");
        dialog.show();
    }
    public long getStartOfDayInMillis(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * @param date the date in the format "yyyy-MM-dd"
     */
    public long getEndOfDayInMillis(String date) throws ParseException {
        // Add one day's time to the beginning of the day.
        // 24 hours * 60 minutes * 60 seconds * 1000 milliseconds = 1 day
        return getStartOfDayInMillis(date) + (24 * 60 * 60 * 1000);
    }

    public String getContactDetails() {
        StringBuffer stringBuffer = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }else {
//            calender.set(2016, calender.NOVEMBER, 18);
//            String fromDate = String.valueOf(calender.getTimeInMillis());
//
//            calender.set(2016, calender.NOVEMBER, 20);
//            String toDate = String.valueOf(calender.getTimeInMillis());
//
//            String[] whereValue = {fromDate,toDate};
            Uri callUri = Uri.parse("content://call_log/calls");
            String[] projection = new String[] {
                    CallLog.Calls._ID,
                    CallLog.Calls.NUMBER,
                    CallLog.Calls.DATE,
                    CallLog.Calls.DURATION,
                    CallLog.Calls.TYPE
            };
            Calendar calendar = Calendar.getInstance();
            String currentDate = String.valueOf(calendar.getTimeInMillis());
            String strOrder1 = android.provider.CallLog.Calls.DATE + " DESC limit 500";
            Cursor cursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI,
                    null,  null,null, CallLog.Calls.DATE +" DESC");
            int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
            int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = cursor.getColumnIndex(CallLog.Calls.DATE);
//            int status=cursor.getColumnIndex(CallLog.Calls.);
            int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
            int name=cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
            while (cursor.moveToNext()) {
                String phn_name=cursor.getString(name);
                String phNumber = cursor.getString(number);
                String callType = cursor.getString(type);
                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = cursor.getString(duration);
                String dir = null;
                int dircode = Integer.parseInt(callType);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;
                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                }
//                list.add(new Cntct_Details(phn_name,phNumber,dir,String.valueOf(callDayTime)));
//                cll_logs.setLayoutManager(new LinearLayoutManager(getActivity()));
//                cll_logs.setAdapter(new CallsAdapter(list,getActivity()));
//                System.out.println("assddffg" + callDayTime+"  "+calendar.getTimeInMillis()+times+times1);
                System.out.println("assddffg" +"current"+ callDayTime.getTime());
//                if (i1==0)
                if (callDayTime.getTime()>=(calendar.getTimeInMillis()-40000000)) {
                    stringBuffer.append("\nPhone Number:--- " + phn_name + phNumber + " \nCall Type:--- "
                            + dir + " \nCall Date:--- " + callDayTime
                            + " \nCall duration in sec :--- " + callDuration + "\n----------------------------------");
//                stringBuffer.append();
//if (i1==0) {
                    System.out.println("assddffg" + callDayTime.getTime());
//    list.add(new Cntct_Details(phn_name, phNumber, dir, String.valueOf(callDayTime),callDuration));
//
//    cll_logs.setLayoutManager(new LinearLayoutManager(getActivity()));
//    CallsAdapter adapter = new CallsAdapter(list, getActivity());
//    cll_logs.setAdapter(adapter);
//    adapter.notifyDataSetChanged();
//                    for (int i=0;i<list.size();i++){
//                        System.out.println("assddffg" + list.get(i).getCallDate());
//                    }

//}else if (i1==1  ){
//    if ( callDayTime.getTime()<(Long.parseLong(times1)-40000000))
//    list1.add(new Cntct_Details(phn_name, phNumber, dir, String.valueOf(callDayTime)));
//    cll_logs.setLayoutManager(new LinearLayoutManager(getActivity()));
//    CallsAdapter adapter = new CallsAdapter(list1, getActivity());
//    cll_logs.setAdapter(adapter);
//    adapter.notifyDataSetChanged();
//}


                }

//                if (i1==1 && callDayTime.getTime()>0){
////                    if (callDayTime.getTime()>=(calendar.getTimeInMillis()-40000000) &&callDayTime.getTime()<=(Long.parseLong(times)-40000000) && callDayTime.getTime()<=(Long.parseLong(times1)-40000000)) {
//                        stringBuffer.append("\nPhone Number:--- " + phn_name + phNumber + " \nCall Type:--- "
//                                + dir + " \nCall Date:--- " + callDayTime
//                                + " \nCall duration in sec :--- " + callDuration + "\n----------------------------------");
////                stringBuffer.append();
//                        System.out.println("assddffg" + callDayTime.getTime()+"  "+calendar.getTimeInMillis());
//                        list.add(new Cntct_Details(phn_name,phNumber,dir,String.valueOf(callDayTime)));
//                        cll_logs.setLayoutManager(new LinearLayoutManager(getActivity()));
//                        CallsAdapter adapter=new CallsAdapter(list,getActivity());
//                        adapter.notifyDataSetChanged();
//                        cll_logs.setAdapter(adapter);
////                    }
//                }
            }
            cursor.close();
            System.out.println("assddffg" + stringBuffer.toString());
        }
        return stringBuffer.toString();
    }

    @Override
    public void onStop() {
        super.onStop();
//        getActivity().unregisterReceiver(bReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

      if (requestCode==REQUEST_CODE){
          if (grantResults.length>0 && grantResults[0]== PackageManager.GET_PERMISSIONS){

          }else {
              Toast.makeText(getActivity(),"You denied the permission", Toast.LENGTH_LONG).show();
          }
      }
    }

//    private BroadcastReceiver  bReceiver = new BroadcastReceiver(){
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            //put here whaterver you want your activity to do with the intent received
//        getContactDetails();
//        }
//    };

//    public void onResume(){
//        super.onResume();
//        LocalBroadcastManager.getInstance(this.getActivity()).registerReceiver(bReceiver, new IntentFilter("message"));
//    }
//
//    public void onPause(){
//        super.onPause();
//        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(bReceiver);
//    }
}
