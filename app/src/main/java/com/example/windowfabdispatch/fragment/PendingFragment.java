package com.example.windowfabdispatch.fragment;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.windowfabdispatch.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingFragment extends AppCompatDialogFragment {


    public PendingFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        getDialog().requestWindowFeature(STYLE_NO_TITLE);
//        getDialog().setCancelable(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_pending, null);
//    }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

//        getDialog().requestWindowFeature(STYLE_NO_TITLE);
//        getDialog().setCancelable(false);
        builder.setView(view).setIcon(R.drawable.elivery_con)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(),"Thank You For Your Response",Toast.LENGTH_SHORT).show();
            }
        });
AlertDialog alertDialog=builder.create();

//        Objects.requireNonNull(alertDialog.getWindow()).setLayout(600, 400);
//    }
        return alertDialog;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
//        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        params.height = 200;
//        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
//    }
}
    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        View view= inflater.inflate(R.layout.fragment_pending, container, false);
//
//        return view;
//    }

//}
