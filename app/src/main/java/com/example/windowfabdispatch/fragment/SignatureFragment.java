package com.example.windowfabdispatch.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windowfabdispatch.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignatureFragment extends Fragment {

SignaturePad signature_pad;
    private Button mClearButton;
    private Button mSaveButton;
TextView signature_pad_description;

    public SignatureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_signature, container, false);
        signature_pad=view.findViewById(R.id.signature_pad);
        mClearButton = (Button) view.findViewById(R.id.clear_button);
        signature_pad_description =  view.findViewById(R.id.signature_pad_description);
        mSaveButton = (Button) view.findViewById(R.id.save_button);
        signature_pad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signature_pad.clear();
            }
        });
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Signed",Toast.LENGTH_SHORT).show();
            }
        });
        signature_pad_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Thank You For Your Response",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

}
