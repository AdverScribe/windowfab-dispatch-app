package com.example.windowfabdispatch.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.example.windowfabdispatch.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {

Button button;
ImageView imageView;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_details, container, false);
button=view.findViewById(R.id.button);
        imageView=view.findViewById(R.id.imageView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPopupMenu();
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openIntent();
            }
        });

        return view;
    }
    public void openIntent(){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:46.7170627,-71.2884537"));
        Objects.requireNonNull(getActivity()).startActivity(intent);
    }
    private void getPopupMenu() {
        PopupMenu menu=new PopupMenu(getActivity(),button);
        menu.getMenuInflater().inflate(R.menu.delivery_menu,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.completed:
                        getFragmentManager().beginTransaction().addToBackStack(null)
                                .replace(R.id.frame_container,new SignatureFragment()).commit();
                        break;
                    case R.id.pending:
PendingFragment fragment=new PendingFragment();
fragment.setCancelable(false);
fragment.show(getFragmentManager(),"pending fragment");
break;
                }
                return false;
            }
        });
        menu.show();
    }

}
