package com.example.windowfabdispatch.constants;

public class LinksAndKeys {
    public static String HEADER_KEY = "";

    public static String BASE_URL = "http://adverscribe.in/windowfab/api/";
    public static String IMAGE_BASE_URL = "";
    public static String BANNERS_URL = "http://demo6150739.mockable.io/svr/getBanners";
    public static String LOGIN_URL = BASE_URL + "user-login";
    public static String CATEGORY_URL = BASE_URL + "category";
    public static String TYPE_URL = BASE_URL + "sub-category";
    public static String PRODUCTS_LIST_URL = BASE_URL + "product";

    public static String NAME_KEY = "name";
    public static String PHONE_KEY = "phone";
    public static String EMAIL_KEY = "email";
    public static String PASSWORD_KEY = "password";
    public static String USER_ID_KEY = "userID";
    public static String CATEGORY_ID_KEY = "category_id";
    public static String SUB_CATEGORY_ID_KEY = "subcategory_id";
    public static String TYPE_ID_KEY = "type_id";
    public static String SELECTED_IMAGE_KEY = "type_id";

    public static String REQUEST_MESSAGE = "Please Wait..";

}
