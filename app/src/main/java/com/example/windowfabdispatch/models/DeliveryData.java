package com.example.windowfabdispatch.models;

public class DeliveryData {

   String packageName,packageCost,PackageTime,PackageStatus;

    public DeliveryData(String packageName, String packageCost, String packageTime, String packageStatus) {
        this.packageName = packageName;
        this.packageCost = packageCost;
        PackageTime = packageTime;
        PackageStatus = packageStatus;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageCost() {
        return packageCost;
    }

    public void setPackageCost(String packageCost) {
        this.packageCost = packageCost;
    }

    public String getPackageTime() {
        return PackageTime;
    }

    public void setPackageTime(String packageTime) {
        PackageTime = packageTime;
    }

    public String getPackageStatus() {
        return PackageStatus;
    }

    public void setPackageStatus(String packageStatus) {
        PackageStatus = packageStatus;
    }
}
